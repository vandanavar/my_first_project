# Dictionaries
my_stuff = {"key1": 123, "key2": "value1",
            "key3": {'123': ['1', '2', 'grabMe']}}
print(my_stuff['key3']['123'][2])

my_other_stuff = {'lunch' : 'pizza', 'bfast': 'eggs'}
my_other_stuff['lunch'] = 'burger'
my_other_stuff['dinner'] = 'pasta'
print(my_other_stuff['lunch'])
print(my_other_stuff)
