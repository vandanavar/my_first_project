from collections import deque
queue = deque()


def print_queue():
    print('present queue is: ', queue)


def add():
    print('enter the element:')
    queue.append(input())
    main_func()


def pop():
    if(len(queue) == 0):
        print('queue is empty')
        main_func()
    print(queue.popleft(), ' is popped from queue')
    main_func()


def main_func():
    print_queue()
    print('enter: ', ' 1 to add elemnt to queue\n',
          '2 to pop an element from queue\n', '3 to exit\n')
    n = int(input())
    if(n == 1):
        add()
    elif(n == 2):
        pop()


main_func()
