# Nested list index
mylist = [4,7,['x', 'h', 'r']]
print(mylist[2][1])
# list comprehension
matrix = [['1','2','3'],['4','5','6'],['7','8','9']]
first_col = [row[0] for row in matrix]
print(first_col)